<?php
if ( ! defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly
}

$GLOBALS['wpsp_template'] = new WPSP_Template_Functions();

global $wpsupportplus, $current_user, $wpdb, $wpsp_template;

$wpsp_user_session = $wpsupportplus->functions->get_current_user_session();
$signup_url        = wp_registration_url();
$signin_url        = $wpsupportplus->functions->get_support_page_url(array('page'=>'sign-in'));
$signout_url       = $wpsupportplus->functions->get_support_page_url(array('page'=>'sign-out'));

?>

<div class="bootstrap-iso">
  
    <script>
    var wpspjq=jQuery.noConflict();
    </script>

    <link href="<?php echo WPSP_PLUGIN_URL.'asset/library/jquery-ui/jquery-ui.min.css?version='.WPSP_VERSION;?>" rel="stylesheet">
    <link href="<?php echo WPSP_PLUGIN_URL.'asset/library/jquery-ui/jquery-ui.structure.min.css?version='.WPSP_VERSION;?>" rel="stylesheet">
    <link href="<?php echo WPSP_PLUGIN_URL.'asset/library/jquery-ui/jquery-ui.theme.min.css?version='.WPSP_VERSION;?>" rel="stylesheet">
    <link href="<?php echo WPSP_PLUGIN_URL.'asset/library/font-awesome/css/font-awesome.min.css?version='.WPSP_VERSION;?>" rel="stylesheet">
    <link href="<?php echo WPSP_PLUGIN_URL.'asset/library/ImageViewer/imageviewer.css?version='.WPSP_VERSION;?>" rel="stylesheet">
    <link href="<?php echo WPSP_PLUGIN_URL.'asset/css/public.css?version='.WPSP_VERSION;?>" rel="stylesheet">
    <!-- <link rel="stylesheet" href="<?php echo WPSP_PLUGIN_URL.'asset/library/jQuery-autoComplete-master/jquery.auto-complete.css?version='.WPSP_VERSION;?>"> -->
    
    <script src="<?php echo WPSP_PLUGIN_URL.'asset/library/tinymce/tinymce.min.js?version='.WPSP_VERSION;?>"></script>
    <script src="<?php echo WPSP_PLUGIN_URL.'asset/library/ImageViewer/imageviewer.min.js?version='.WPSP_VERSION;?>" type="text/javascript"></script>
    <!-- <script src="<?php echo WPSP_PLUGIN_URL.'asset/library/jQuery-autoComplete-master/jquery.auto-complete.js?version='.WPSP_VERSION;?>"></script> -->
    <!-- Bootstrap setting -->
    
    <script src="<?php echo WPSP_PLUGIN_URL.'asset/js/public.js?version='.WPSP_VERSION;?>"></script>
    
    <?php do_action('wpsp_enqueue_scripts');?>

    <script>
    var wpsp_data = <?php $wpsp_template->js_data()?>;
    var link = true;
    </script>
    <style type="text/css">
          
          .modal-backdrop{
            display: none !important;
          }
          
         <?php 
         // Custom CSS
         echo stripslashes($wpsupportplus->functions->get_custom_css());
         echo stripslashes($wpsupportplus->functions->get_theme_intrgration());
         
         //Customize General
         $customize_general = $wpsupportplus->functions->get_customize_general();
         foreach ($customize_general as $css) {
            echo stripslashes($css);
         }
         
         //Ticket List
         $customize_ticket_list = $wpsupportplus->functions->get_customize_ticket_list();
         foreach ($customize_ticket_list as $css) {
            echo stripslashes($css);
         }
         
         do_action('wpsp_custom_css');
         ?>
         
    </style>

    <?php
    $current_page = isset($_REQUEST['page']) ? sanitize_text_field($_REQUEST['page']) : $wpsp_template->default_support_page();

    switch ($current_page){
        case 'tickets': include( WPSP_ABSPATH . 'template/tickets/tickets.php' );
            break;
        case 'sign-in': include( WPSP_ABSPATH . 'template/header/sign-in.php' );
            break;
        case 'sign-out': include( WPSP_ABSPATH . 'template/header/sign-out.php' );
            break;
        default : do_action( 'wpsp_include_page', $current_page);
            break;
    }

    ?>
    <div style="margin-bottom:40px;"></div>

</div>
<?php
