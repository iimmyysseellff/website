<?php
/**
 * Created by PhpStorm.
 * User: khairulimam
 * Date: 27/02/18
 * Time: 0:48
 */
add_theme_support('post-thumbnails');

/**
 * Filter the except length to 20 words.
 *
 * @param int $length Excerpt length.
 * @return int (Maybe) modified excerpt length.
 */
function wpdocs_custom_excerpt_length($length)
{
  return 23;
}

add_filter('excerpt_length', 'wpdocs_custom_excerpt_length', 999);

function my_login_logo()
{ ?>
  <style type="text/css">
    #login h1 a, .login h1 a {
      background-image: url("https://i.imgur.com/DVqgoMG.png");
      height: 84px;
      width: 84px;
      background-size: 84px;
      background-repeat: no-repeat;
      padding-bottom: 30px;
    }
  </style>
<?php }

add_action('login_enqueue_scripts', 'my_login_logo');