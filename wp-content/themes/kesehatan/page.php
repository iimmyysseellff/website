<?php get_header() ?>
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-lg-offset-2 add-martop">
        <?php
        if (have_posts()) : while (have_posts()) : the_post();

          get_template_part('page-content', get_post_format());

        endwhile; endif;
        ?>
      </div>
    </div>
  </div>
<?php get_footer() ?>