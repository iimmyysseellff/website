<div class="footer">
  <div class="container">
    <div class="text-muted text-center">
      &copy; Copyright Dinas Kesehatan Lombok Utara
    </div>
  </div>
</div>

<script src="<?php echo get_theme_file_uri() ?>/bootstrap/js/jquery-3.2.1.min.js"></script>
<!--bootstrap.min.js loaded on wp live chat plugin. No need to import it here-->
<!--<script src="--><? //= get_theme_file_uri() ?><!--/bootstrap/js/bootstrap.min.js"></script>-->
<script src="<?php echo get_theme_file_uri() ?>/bootstrap/js/jquery.smartmenus.js"></script>
<script src="<?php echo get_theme_file_uri() ?>/bootstrap/js/jquery.smartmenus.bootstrap.js"></script>
<?php wp_footer(); ?>
<script type='text/javascript' data-cfasync='false'>window.purechatApi = {
    l: [], t: [], on: function () {
      this.l.push(arguments);
    }
  };
  (function () {
    var done = false;
    var script = document.createElement('script');
    script.async = true;
    script.type = 'text/javascript';
    script.src = 'https://app.purechat.com/VisitorWidget/WidgetScript';
    document.getElementsByTagName('HEAD').item(0).appendChild(script);
    script.onreadystatechange = script.onload = function (e) {
      if (!done && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete')) {
        var w = new PCWidget({c: '2e7dea4e-95fa-49c4-9628-b4efb6d0c157', f: true});
        done = true;
      }
    };
  })();</script>
</body>
</html>