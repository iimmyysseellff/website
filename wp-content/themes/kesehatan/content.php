<div class="container">
  <div class="row">
    <div class="col-lg-offset-2 col-lg-8 martop-content">
      <div class="media">
        <div class="media-left">
          <a href="#">
            <img class="media-object img-circle" src="<?php echo get_theme_file_uri() ?>/img/logoklu.png" width="64"
                 height="64"
                 alt="Picture">
          </a>
        </div>
        <div class="media-body">
          <h4 class="media-heading">
            <?php echo get_the_author_meta('first_name') ?> <?php echo get_the_author_meta('last_name') ?>
          </h4>
          <p><?php echo get_the_author_meta('description') ?></p>
          <p><?php echo the_date() ?></p>
        </div>
      </div>
      <h3><?php the_title(); ?></h3>
      <p>
        <?php the_content(); ?>
      </p>
      <hr>
      <?php
      if (comments_open() || get_comments_number()) :
        comments_template();
      endif;
      ?>
    </div>
  </div>
</div>