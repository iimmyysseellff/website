<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="Dinas Kesehatan Lombok Utara">
  <meta name="author" content="Khairul Imam">
  <meta https-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="<?php echo get_theme_file_uri() ?>/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo get_theme_file_uri() ?>/bootstrap/css/bootstrap-theme.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <link href="<?php echo get_theme_file_uri() ?>/bootstrap/css/smartmenus.bootstrap.css" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo get_theme_file_uri() ?>/bootstrap/css/footer.css">
  <link rel="stylesheet" href="<?php echo get_theme_file_uri() ?>/bootstrap/css/style.css">
  <title><?php echo get_bloginfo('name'); ?></title>
  <?php wp_head(); ?>
  <link rel="icon" href="<?php echo get_theme_file_uri() ?>/img/icon.png" sizes="50x50" />
</head>
<body>
<nav class="navbar navbar-default navbar-fixed-top">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
              data-target="#bs-example-navbar-collapse-1"
              aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="<?php echo get_bloginfo('wpurl') ?>">Dinkes Lombok Utara</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <form class="navbar-form navbar-right">
        <div class="form-group has-feedback">
          <input type="text" class="form-control" placeholder="Ada yang ingin dicari?" id="inputSuccess2"
                 aria-describedby="inputSuccess2Status">
          <span class="glyphicon glyphicon-search form-control-feedback" aria-hidden="true"></span>
          <span id="inputSuccess2Status" class="sr-only">(success)</span>
        </div>
        <!-- /input-group -->
      </form>
      <ul class="nav navbar-nav">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
             aria-expanded="false">Profile
            <span class="caret"></span>
          </a>
          <ul class="dropdown-menu">
            <li>
              <a href="#">Tugas dan Fungsi</a>
            </li>
            <li>
              <a href="#">Visi dan Misi</a>
            </li>
            <li>
              <a href="#">Profile Pejabat dan Struktrual</a>
            </li>
            <li>
              <a href="#">Profile Pegawa</a>
            </li>
            <li>
              <a href="#">Struktur Organisasi</a>
            </li>
          </ul>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
             aria-expanded="false">Program
            <span class="caret"></span>
          </a>
          <ul class="dropdown-menu">
            <li>
              <a href="#">Program Unggulan
                <span class="caret"></span>
              </a>
              <ul class="dropdown-menu">
                <li>
                  <a href="#">Program 1</a>
                </li>
                <li>
                  <a href="#">Program 2</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="#">Program Tahunan</a>
            </li>
            <li>
              <a href="#">Agenda Kegiatan</a>
            </li>
            <li>
              <a href="#">Kerjasama</a>
            </li>
          </ul>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
             aria-expanded="false">Bidang
            <span class="caret"></span>
          </a>
          <ul class="dropdown-menu">
            <li>
              <a href="#">Sekretariat</a>
            </li>
            <li>
              <a href="#">Kesmas</a>
            </li>
            <li>
              <a href="#">P2PL</a>
            </li>
            <li>
              <a href="#">Pelayanan Kesehatan</a>
            </li>
          </ul>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
             aria-expanded="false">UPTD
            <span class="caret"></span>
          </a>
          <ul class="dropdown-menu">
            <li>
              <a href="#">Rumah Sakit</a>
            </li>
            <li>
              <a href="#">Puskesmas</a>
            </li>
          </ul>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
             aria-expanded="false">Layanan Publik
            <span class="caret"></span>
          </a>
          <ul class="dropdown-menu">
            <li>
              <a href="#">Data Puskemas</a>
            </li>
            <li>
              <a href="#">Data Rumah Sakit</a>
            </li>
            <li>
              <a href="#">ePengaduan</a>
            </li>
            <li>
              <a href="#">PPID</a>
            </li>
            <li>
              <a href="#">SOP</a>
            </li>
            <li>
              <a href="#">BPJS</a>
            </li>
            <li>
              <a href="#">Laporan
                <span class="caret"></span>
              </a>
              <ul class="dropdown-menu">
                <li>
                  <a href="#">DIPA</a>
                </li>
                <li>
                  <a href="#">Rencana Kerjasama</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="#">Berita dan Artikel</a>
            </li>
          </ul>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
             aria-expanded="false">Download
            <span class="caret"></span>
          </a>
          <ul class="dropdown-menu">
            <li>
              <a href="#">Profile Kesehatan</a>
            </li>
            <li>
              <a href="#">Regulasi</a>
            </li>
            <li>
              <a href="#">eBook</a>
            </li>
            <li>
              <a href="#">Promosi Kesehatan</a>
            </li>
            <li>
              <a href="#">Materi Pertemuan</a>
            </li>
          </ul>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
             aria-expanded="false">Aplikasi
            <span class="caret"></span>
          </a>
          <ul class="dropdown-menu">
            <li>
              <a href="#">Aplikasi 1</a>
            </li>
            <li>
              <a href="#">Aplikasi 2</a>
            </li>
            <li>
              <a href="#">Aplikasi 3</a>
            </li>
            <li>
              <a href="#">Aplikasi Lain-Lain</a>
            </li>
          </ul>
        </li>
      </ul>
    </div>
    <!-- /.navbar-collapse -->
  </div>
  <!-- /.container-fluid -->
</nav>