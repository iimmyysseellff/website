<?php
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
$temp = $wp_query;
$wp_query = null;
$wp_query = new WP_Query();
$wp_query->query('posts_per_page=50&post_type=post&paged=' . $paged . '&category_name="Video Kesehatan"');

//$search = new WP_Query('category_name="Peraturan OJK"');
$total_results = $wp_query->found_posts;

get_header()
?>
<div class="container">
  <div class="page-header">
    <h1>Video Kesehatan</h1>
  </div>
</div>
<section id="content">
  <div class="container">
    <?php if ($wp_query->have_posts()): ?>
      <div class="row">
        <div class="col-lg-12">

          <?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
            <div class="col-sm-6 col-md-4">
              <a href="<?php the_permalink() ?>" title="<?php echo the_title() ?>" class="thumbnail">
                <img style="height: 190px"
                     src="<?php if (has_post_thumbnail()): the_post_thumbnail_url(); else: ?><?php echo get_theme_file_uri() ?>/img/th.svg<?php endif; ?>"
                     alt="Thumbnail">
              </a>
            </div>
          <?php endwhile; ?>
        </div>
      </div>
      <div class="row">
        <nav>
          <?php
          global $wp_query;

          $big = 999999999; // need an unlikely integer
          echo '<ul class="pager">';
          echo paginate_links(array(
              'base' => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
              'format' => '?paged=%#%',
              'prev_text' => __('<<'),
              'next_text' => __('>>'),
              'current' => max(1, get_query_var('paged')),
              'total' => $wp_query->max_num_pages
          ));
          echo '</ul>';
          ?>
        </nav>
      </div>
    <?php endif; ?>
  </div>
</section>
<?php
get_footer();
?>
