<?php get_header() ?>
<div class="container">
  <div class="row">
    <div class="col-lg-8 col-lg-offset-2 add-martop">
      <p align="justify">
        <?php
        if (have_posts()) : while (have_posts()) : the_post();

          get_template_part('page-content', get_post_format());

        endwhile; endif;
        ?>
      </p>
    </div>
  </div>
</div>
<div class="container">
  <div class="row">
    <div class="col-lg-8 col-lg-offset-2">
      <div id="disqus_thread"></div>
    </div>
  </div>
</div>
<script>

  /**
   *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
   *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
  /*
  var disqus_config = function () {
  this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
  this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
  };
  */
  (function () { // DON'T EDIT BELOW THIS LINE
    var d = document, s = d.createElement('script');
    s.src = 'https://dinas-kesehatan-lombok-utaram.disqus.com/embed.js';
    s.setAttribute('data-timestamp', +new Date());
    (d.head || d.body).appendChild(s);
  })();
</script>
<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by
    Disqus.</a></noscript>
<script id="dsq-count-scr" src="//dinas-kesehatan-lombok-utaram.disqus.com/count.js" async></script>
<?php get_footer() ?>
