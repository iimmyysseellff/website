<?php get_header(); ?>
<?php
$wp_query = new WP_Query();
?>
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">
      <div class="item active">
        <img src="<?php echo get_theme_file_uri() ?>/img/banners/b1.jpeg" alt="Banner">
      </div>

      <div class="item">
        <img src="http://gdaha.org/wp-content/uploads/2016/03/aug-plaza-gdaha.jpg" alt="Chicago">
      </div>

      <div class="item">
        <img src="http://kinsleyconstruction.com/wp-content/uploads/2017/06/IMG_036600-1600x700.jpg" alt="New York">
      </div>
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>

  <div class="container" style="margin-top: 20px;">
    <div class="jumbotron">
      <div class="container">
        <div class="row">
          <div class="col-sm-4">
            <img class="img-circle" width="100%" height="100%"
                 src="http://avignalearning.com/wp-content/uploads/2016/01/girl-callcenter.png"
                 alt="">
          </div>
          <div class="col-sm-8">
            <h1>Konsultasi Kapan Saja</h1>
            <p>Kami menyediakan layanan konsultasi kapan saja 24x7 untuk memaksimalkan layanan kesehatan kepada seluruh
              masyarakat</p>
            <p>
              <button class="btn btn-success" data-toggle="modal" data-target="#deskripsiObrolanUmum">Obrolan
                Umum
              </button>
              <button class="btn btn-primary" data-toggle="modal" data-target="#deskripsiKonsultasiPribadi">Konsultasi
                Kesehatan
              </button>
              <button class="btn btn-danger" data-toggle="modal" data-target="#deskripsiPengaduanLayanan">Pengaduan
                Layanan
              </button>
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <div class="panel-default panel">
          <div class="panel-heading">
            <h3 class="panel-title">
              Info Grafis
            </h3>
          </div>
          <div class="panel-body">
            <ul class="nav nav-tabs">
              <li class="active">
                <a href="#efektif" data-toggle="tab">
                  <i class="icon-briefcase"></i> Promosi Kesehatan</a>
              </li>
              <li class="">
                <a href="#flat" data-toggle="tab"> Capaian Program</a>
              </li>
              <li class="">
                <a href="#anuitas" data-toggle="tab"> Serapan Anggaran</a>
              </li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="efektif">
                <div id="myCarousel1" class="carousel slide" data-ride="carousel">
                  <!-- Indicators -->
                  <ol class="carousel-indicators">
                    <li data-target="#myCarousel1" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarousel1" data-slide-to="1"></li>
                    <li data-target="#myCarousel1" data-slide-to="2"></li>
                  </ol>

                  <!-- Wrapper for slides -->
                  <div class="carousel-inner">
                    <div class="item active">
                      <img src="https://www.beyondwalls.blog/wp-content/uploads/2017/02/post2-entrance-1200x700-01.jpg"
                           alt="Los Angeles">
                    </div>

                    <div class="item">
                      <img
                          src="http://www.cypressmillwork.com/sites/cypressmillwork.com/files/styles/1200x700/public/Houston%20Millwork.jpg?itok=ktkCwCNB"
                          alt="Chicago">
                    </div>

                    <div class="item">
                      <img src="https://cdn.extra.ie/wp-content/uploads/2018/01/25110557/wexford-general-hospital.jpg"
                           alt="New York">
                    </div>
                  </div>

                  <!-- Left and right controls -->
                  <a class="left carousel-control" href="#myCarousel1" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                    <span class="sr-only">Previous</span>
                  </a>
                  <a class="right carousel-control" href="#myCarousel1" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                    <span class="sr-only">Next</span>
                  </a>
                </div>
              </div>
              <div class="tab-pane" id="flat">
                <div id="myCarousel2" class="carousel slide" data-ride="carousel">
                  <!-- Indicators -->
                  <ol class="carousel-indicators">
                    <li data-target="#myCarousel2" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarousel2" data-slide-to="1"></li>
                    <li data-target="#myCarousel2" data-slide-to="2"></li>
                  </ol>

                  <!-- Wrapper for slides -->
                  <div class="carousel-inner">
                    <div class="item active">
                      <img src="https://www.upcapitalmgmt.com/wp-content/uploads/2017/03/SPX-MA50-MA200.png"
                           alt="Los Angeles">
                    </div>

                    <div class="item">
                      <img src="https://www.upcapitalmgmt.com/wp-content/uploads/2017/03/XLV-MA50-MA200.png"
                           alt="Chicago">
                    </div>

                    <div class="item">
                      <img
                          src="http://www.forex-owl.com/wp-content/uploads/2016/04/EUR-CHF-Price-Event-Chart-1200x700.png"
                          alt="New York">
                    </div>
                  </div>

                  <!-- Left and right controls -->
                  <a class="left carousel-control" href="#myCarousel2" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                    <span class="sr-only">Previous</span>
                  </a>
                  <a class="right carousel-control" href="#myCarousel2" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                    <span class="sr-only">Next</span>
                  </a>
                </div>
              </div>
              <div class="tab-pane" id="anuitas">
                <div id="myCarousel3" class="carousel slide" data-ride="carousel">
                  <!-- Indicators -->
                  <ol class="carousel-indicators">
                    <li data-target="#myCarousel3" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarousel3" data-slide-to="1"></li>
                    <li data-target="#myCarousel3" data-slide-to="2"></li>
                  </ol>

                  <!-- Wrapper for slides -->
                  <div class="carousel-inner">
                    <div class="item active">
                      <img
                          src="https://lokadata.beritagar.id/storage/charts/img/anggaran-kesehatan-pada-apbn-2014-2018-1518071675.jpg"
                          alt="Los Angeles">
                    </div>

                    <div class="item">
                      <img
                          src="https://d3fy651gv2fhd3.cloudfront.net/charts/saudi-arabia-government-budget.png?s=saudiarabiagb&v=201712261700v&lang=all"
                          alt="Chicago">
                    </div>

                    <div class="item">
                      <img
                          src="http://www.forex-owl.com/wp-content/uploads/2016/04/EUR-CHF-Price-Event-Chart-1200x700.png"
                          alt="New York">
                    </div>
                  </div>

                  <!-- Left and right controls -->
                  <a class="left carousel-control" href="#myCarousel3" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                    <span class="sr-only">Previous</span>
                  </a>
                  <a class="right carousel-control" href="#myCarousel3" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                    <span class="sr-only">Next</span>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-sm-8">
        <div class="row">
          <div class="col-sm-12">
            <div class="panel-default panel">
              <div class="panel-heading">
                <h3 class="panel-title">
                  <a href="<?php echo get_bloginfo('wpurl') ?>/index.php/berita-terkini">Berita Terkini <small>(Lihat semua berita)</small></a>
                </h3>
              </div>
              <div class="panel-body">
                <?php $the_query = new WP_Query('posts_per_page=3&post_type="post"&category_name="Berita"'); ?>
                <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>
                  <div class="col-sm-6 col-md-4">
                    <div class="thumbnail">
                      <img style="height: 170px;"
                           src="<?php if (has_post_thumbnail()): the_post_thumbnail_url(); else: ?><?php echo get_theme_file_uri() ?>/img/th.svg<?php endif; ?>"
                           alt="Thumbnail">
                      <div class="caption">
                        <h3>
                          <a href="<?php echo the_permalink() ?>"><?php echo the_title() ?></a>
                        </h3>
                        <p><?php echo the_excerpt() ?></p>
                      </div>
                    </div>
                  </div>
                <?php
                endwhile;
                wp_reset_postdata();
                ?>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="row">
          <div class="col-sm-12">
            <div class="panel-default panel">
              <div class="panel-heading">
                <h3 class="panel-title">
                  <a href="<?php echo get_bloginfo('wpurl') ?>/index.php/pengumuman">
                    Pengumuman <small>(Lihat semua pengumuman)</small>
                  </a>
                </h3>
              </div>
              <div class="list-group">
                <?php $the_query = new WP_Query('posts_per_page=6&post_type="post"&category_name="Pengumuman"'); ?>
                <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>
                  <a href="<?php echo the_permalink() ?>" class="list-group-item"><span
                        class="fa fa-bullhorn text-primary"></span> <?php echo the_title() ?></a>
                <?php
                endwhile;
                wp_reset_postdata();
                ?>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-12">
            <div class="panel-default panel">
              <div class="panel-heading">
                <h3 class="panel-title">
                  Media Sosial
                </h3>
              </div>
              <div class="list-group">
                <a href="https://www.facebook.com/Dinas-Kesehatan-Lombok-Utara-1943430802639782/" target="_blank"
                   class="list-group-item">
                  <span class="fa fa-facebook-f text-primary"></span> Facebook</a>
                <a href="https://twitter.com/DinkesKlu" target="_blank" class="list-group-item">
                  <span class="fa fa-twitter text-info"></span> Twitter</a>
                <a href="#" class="list-group-item">
                  <span class="fa fa-instagram text-primary"></span> Instagram</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="container">
    <div class="panel-default panel">
      <div class="panel-heading">
        <h3 class="panel-title">
          <a href="<?php echo get_bloginfo('wpurl') ?>/index.php/video-kesehatan">Video Kesehatan <small>(Lihat semua video)</small></a>
        </h3>
      </div>
      <div class="panel-body">
        <?php
        $wp_query->query('posts_per_page=4&post_type=post&category_name="Video Kesehatan"');
        while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
          <div class="col-sm-6 col-md-3">
            <a href="<?php the_permalink() ?>" data-toggle="tooltip" title="<?php echo the_title() ?>. Pada: <?php echo get_the_date() ?>" class="thumbnail img-responsive">
              <img style="height: 170px;"
                   src="<?php if (has_post_thumbnail()): the_post_thumbnail_url(); else: ?><?php echo get_theme_file_uri() ?>/img/th.svg<?php endif; ?>"
                   alt="Thumbnail">
            </a>
          </div>
        <?php
        endwhile;
        wp_reset_postdata();
        ?>
      </div>
    </div>
  </div>
  <div class="container">
    <?php include_once 'gpr-kominfo.php' ?>
    <div class="panel-default panel">
      <div class="panel-heading">
        <h3 class="panel-title">
          Government Public Relations (GPR)
        </h3>
      </div>
      <div class="panel-body">
        <?php
        foreach ($feeds as $feed_): ?>
          <div class="col-sm-6 col-md-3">
            <a href="<?php echo $feed_['link'] ?>" target="_blank" data-toggle="tooltip" title="<?php echo $feed_['title'] ?>. Pada: <?php echo $feed_['pubDate'] ?>"
               class="thumbnail img-responsive">
              <img style="height: 170px;"
                   src="https://widget.kominfo.go.id/images/artikelberitagpr.png"
                   alt="Thumbnail">
            </a>
          </div>
        <?php
        endforeach;
        ?>
      </div>
    </div>
  </div>


  <!-- Modals -->
  <div class="modal fade" id="deskripsiObrolanUmum" tabindex="-1" role="dialog"
       aria-labelledby="deskripsiObrolanUmumLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h4 class="modal-title" id="myModalLabel">Obrolan Umum</h4>
        </div>
        <div class="modal-body">
          Obrolan Umum adalah layanan tanya jawab yang ditujukan untuk publik, setiap pertanyaan yang diajukan pada
          obrolan umum akan
          ditampilkan secara publik, oleh karena itu semua percakapan pada obrolan umum sebaiknya tidak mengandung
          rincian
          pribadi untuk menghindari ketidak nyamanan
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
          <a href="<?php echo get_bloginfo('wpurl') ?>/index.php/obrolan-umum/" target="_blank" class="btn btn-success">Mulai
            Obrolan</a>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="deskripsiKonsultasiPribadi" tabindex="-1" role="dialog"
       aria-labelledby="deskripsiKonsultasiPribadiLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h4 class="modal-title" id="myModalLabel">Konsultasi Kesehatan</h4>
        </div>
        <div class="modal-body">
          Konsultasi Kesehatan adalah layanan konsultasi online yang ditujukan untuk menjawab pertanyaan-pertanyaan yang
          bersifat pribadi
          seputar kesehatan, sebagai dukungan program berantas Gizi Buruk Pemerintah Lombok Utara. Tidak seperti Obrolan
          Umum, semua percakapan pada layanan ini hanya akan bisa dilihat oleh penanya dan admin
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
          <a href="https://app.purechat.com/w/Konsultasi%20Kesehatan" target="_blank" class="btn btn-primary openchat">Mulai
            Konsultasi</a>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="deskripsiPengaduanLayanan" tabindex="-1" role="dialog"
       aria-labelledby="deskripsiPengaduanLayananLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h4 class="modal-title" id="myModalLabel">Pengaduan Layanan</h4>
        </div>
        <div class="modal-body">
          Pengaduan Layanan adalah kotak saran untuk memberikan timbal balik sebagai tolok ukur Dinas Kesehatan agar
          memberikan layanan
          yang lebih baik
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
          <a href="<?php echo get_bloginfo('wpurl') ?>/index.php/pengaduan-layanan/" target="_blank"
             class="btn btn-danger">Mulai
            Pengaduan</a>
        </div>
      </div>
    </div>
  </div>

  <div class="footer-information">
    <iframe id="peta"
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3947.282198428843!2d116.27552531412694!3d-8.373877686741846!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dcdd10000023f13%3A0x78e7b74dc2a820fb!2sDinas+Kesehatan+Kabupaten+Lombok+Utara!5e0!3m2!1sen!2sid!4v1518839394907"
            width="100%" height="510" frameborder="0" style="border:0;padding-bottom: 20px" allowfullscreen></iframe>
    <div class="container">
      <div class="row">
        <div class="col-md-4">
          <h3 class="text-center text-primary">Kontak</h3>
          <p align="center">Alamat Kantor Utama: JL. Raya Tanjung Bayan, Rempek, Gangga, Kabupaten Lombok Utara
            <br>Kode Pos : 83353
            <br>Email : dikes@server.go.id
            <br>Telpon : (0370) – 948849</p>
        </div>
        <div class="col-md-4">
          <!--          <h3 class="text-center text-primary">Twitter Feed</h3>-->
          <a class="twitter-timeline" data-lang="id" data-width="100%" data-height="300" data-dnt="true"
             href="https://twitter.com/DinkesKlu?ref_src=twsrc%5Etfw">Tweets by DinkesKlu</a>
          <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
        </div>
        <div class="col-md-4">
          <?php echo the_widget('SimpleVisitorCounter_Widget') ?>
        </div>
      </div>
    </div>
  </div>
<?php get_footer() ?>
<script>
  jQuery(document).ready(function(){
    jQuery('[data-toggle="tooltip"]').tooltip();
  });
</script>
