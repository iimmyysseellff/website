<?php
/**
 * Created by PhpStorm.
 * User: khairulimam
 * Date: 03/03/18
 * Time: 2:51
 */

$feed = file_get_contents("https://widget.kominfo.go.id/data/latest/gpr.xml");
$rss = simplexml_load_string($feed);
$feeds = array();
for ($i = 0; $i <= 3; $i++) {
  $object = $rss->item[$i];
  $newFeed = array();
  foreach ($object as $key => $value) {
    $newFeed[$key] = sprintf($value);
  }
  $feeds[$i] = $newFeed;
}
$jsonencoded = json_encode($feeds);